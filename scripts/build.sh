#!/bin/bash

aulas=(
	01_introducao
	02_git
	03_docker
	04_cicd
)

for i in ${aulas[@]}
do
  echo ">>> ${i}"
  cd ${i}
  if make
  then
    if [ -f *.html ]
    then
      cp *.html ../public/
      echo "cp *.html ../public/ ... ok"
    fi
    ls -1 *.pdf > /dev/null 2>&1
    if [ "$?" = "0" ]
   # if [ -f *.pdf ]
    then
      cp *.pdf ../public/
      echo "cp *.pdf ../public/ ... ok"
    fi
    if [ -d img ] 
    then
      if [ ! -d ../public/img ]
      then
        mkdir ../public/img
      fi
      cp -r img/* ../public/img/
      echo "cp -r img/* ../public/img ... ok"
    fi
  fi
  echo "<<< ${i}"
  cd -
done

pandoc index.md -o ./public/index.html

exit 0
