Especialização em Desenvolvimento Ágil de Software

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

*Infra-estrutura para desenvolvimento e implantação de Software (DevOps)*

Prof. Alexander Robert Kutzke

# Material do módulo Infra-estrutura para desenvolvimento e implantação de Software (DevOps)

## Aulas

* Aula 01 - Introdução ao DevOps  [[html](introducao.html)] [[pdf](introducao.pdf)];
* Aula 02 - Git não tão simples [[html](git.html)] [[pdf](git.pdf)];
* Aula 03 - Introdução ao Docker [[pdf](intro_docker.pdf)];
* Aula 03 - Docker Essencial [[html](docker.html)] [[pdf](docker.pdf)];
* Aula 04 - CI/CD com Gitlab Pipelines [[html](cicd.html)] [[pdf](cicd.pdf)];
