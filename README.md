Especialização em Desenvolvimento Ágil de Software

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

*Infra-estrutura para desenvolvimento e implantação de Software (DevOps)*

Prof. Alexander Robert Kutzke

# Material do módulo Infra-estrutura para desenvolvimento e implantação de Software (DevOps)

## Aulas

* [Aula 01 - Introdução ao DevOps](01_introducao/slides/introducao.md);
* [Aula 02 - Git não tão simples](02_git/slides/git.md);
* [Aula 03 - Introdução ao Docker](03_docker/intro_docker.md);
* [Aula 03 - Docker essencial](03_docker/docker.md);
* [Aula 04 - Docker essencial](04_cicd/cicd.md);

## Exercícios avaliativos

- [Exercício 01 - Gitlab Pipeline + Git flow](exercicios_avaliativos/exercicio_01.md);
- [Exercício 02 - Docker](https://gitlab.com/das-alexkutzke/docker-exercises);
- [Exercício 03 - App nodeJS e Pipeline](https://gitlab.com/das-alexkutzke/fibonacci-cli);

## Outros

- [Links interessantes](./links.md).
