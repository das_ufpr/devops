% DevOps: Uma introdução
% Alex Kutzke
% Março 2022

# Infra-estrutura para desenvolvimento e implantação de Software (DevOps)

## Ementa

* Estudo e aplicação de técnicas ágeis para o desenvolvimento e implantação de softwares;
* Gerenciamento de ciclo de vida de aplicações;
* Sistemas de controle de versões;
* Infrastructure as code;
* Integração e entrega Contínuas (CI/CD);
* Técnicas de build e deploy automáticos; 
* Micro-serviços. Conteinerização;
* Testes automatizados.

## Mas o que estudaremos?

- Fundamentos do **DevOps**;
- Mexer com ferramentas :)
  - Git (e Gitlab);
  - Docker;
  - Pipelines;
  - Terraform?
  - Oracle Cloud.

## Semanas

* Semana 1:
  - O que é DevOps??
  - Revisão de Git;
  - Git flows;
  - Gitlab Pipeline (exemplo beeeem básico);
- Semana 2:
  - Docker!
  - Deploy manual?
  - Oracle Cloud?

## Semanas (cont.)

- Semana 3 (?):
  - Gitlab Pipeline novamente (?);
  - Cloud?
  - Terraform (Iac)?
  - Kubernetes?


# DevOps

## Definição (ões)

* Segundo [Tom Geraghty](https://tomgeraghty.co.uk/index.php/the-history-and-evolution-of-devops/):

> "DevOps talvez seja um dos conceitos mais falados nos tempos atuais da 
> industria de tecnologia. Entretanto, o que esse termo realmente significa 
> ainda é tópico de muito debate: alguns descrevem DevOps como uma cultura 
> de melhoramento de processos, enquanto outros o descrevem como puramente 
> termos tecnológicos de automação e de tecnologias em nuvem."

## Origem: Dev X Ops

- Durante muito tempo tecnologia foi dividida entre:
  - **Dev's**: aqueles que "criam":
    - Escrevem código;
  - **Ops's**: aqueles que fazem "*build* e manutenção":
    - **Build** e mantem o sistema rodando.

## Relação antagônica?

- **Devs** são avaliados e motivados pela grande quantidade de alterações de código e novas funcionalidades;
- Para **Ops**, mudanças frequentes seriam ruins, pois causam instabilidade e dificuldades de manutenção (será?);
- Times separados e em constante discussão;

## Uma definição inicial

> "**DevOps** é ou, pelo menos, iniciou como um esforço de reconciliação 
> dessa fratura (entre os times de desenvolvimento e de operações) e de 
> melhora no desempenho dos negócios."

## Principal foco

> Foco em desenvolvimento incremental e entrega rápida.

Mas como?

## Objetivos específicos

- Cultura;
- Processos;
- Velocidade;
- Ciclos de feedback;
- Repetibilidade por meio de automação;
- Responsividade à mudanças;
- Melhora contínua.

## Ou melhor: CALMS

- **C**ulture;
- **A**utomation;
- **L**ean (IT);
- **M**easurement;
- **S**haring;

"Manual do DevOps" - Gene Kim, Jez Humble, John Willis, Patrick Debois

## Nem tão novo assim

Sistemas de **produção** evoluem:

- Taylorismo (1950): melhorou muito produtividade, mas qualidade caiu:
  - Defeitos eram passados para frente em linhas de produção;
  - Funcionários treinados para ignorar falhas e atingir metas de produtividade;

## Nem tão novo assim (cont.)

- William Edwards Deming adicionou a análise estatística aos processos de produção:
  - Ciclo de Deming: "Plan - Do - Check - Act";
  - Similar ao ciclo de vida atual de produção de software;
  - Foco em qualidade;
  - **Análise contínua** e **melhoramento de processos**:
    - Pontos importantes do DevOps;

## Conhecer o processo

> “Any improvements made anywhere besides the bottleneck are an illusion.” 
- Eliyahu M. Goldratt

## Pessoas e processos

- Alienação no processo de produção:
  - Diferença entre um artesão que faz uma cadeira inteira e um funcionário de linha de produção que apenas encaixa duas pernas na cadeira;
  - Funcionário nunca vê a cadeira completa;

- Pensamento sistemático:
  - Capacidade de tomar pequenas decisões mas compreender os impactos no sistema como um todo;
  - Não é uma ferramenta ou um processo pronto;
  - É uma prática cultural.

## DevOps e Agile Devlopment

- Coisas diferentes;
- Mas com objetivos em comum;
- Um é feito com mais facilidade quando na presença do outro.

## Primeiro DevOps

- Google em rápida expansão;
- Necessidade de incorporar rapidamente novas funcionalidades enquanto mantinham uma plataforma gigantesca em funcionamento;
- Time "*Site Reliability Engineering (SRE)*", liderado por Ben Traynor:
  - Metade do trabalho era encontrar problemas e realização de manutenção;
  - Outra metade era desenvolvimento de novas funcionalidades, melhoramentos e automações.

- **Meio que** deu origem ao Kubernetes :)

## Tecnologia de Nuvem

- Ampliou possibilidades (principalmente de automação);
- Novas ferramentas;
- Infrastructure as Code (IoC);
- Entrega contínua (*Continuous Delivery*);

- CI/CD ?

## CI/CD

- Integração e entrega contínuas;
- ... ou tudo que isso envolve:
  - Versionamento de código:
    - Fluxos de desenvolvimento;
  - Testes contínuos (testes unitários e vários outros);
  - Integração rápida e frequente;
  - Gerenciamento de configuração;
  - Observabilidade;
  - ...;

## Ciclo de Vida do DevOps

![Fonte: GITLAB, 2022](img/devops_lifecycle.png)

Secure: *DevSecOps*!

## Ferramentas para o DevOps

- **Gerenciamento de Projetos**: Git, GitHub, GitLab, Jira ...;
- **Repositórios de código compartilhados**: Git, GitHub, GitLab, ...;
- **CI/CD**: Jenkins, CircleCI, Git Hooks, GitHub Actions, GitLab Pipelines, ...;
- **Testes**: Geralmente específicos de cada plataforma/Framework;
- **Gerenciamento de configurações** (IaC): Ansible, Chef, Terraform, ...;
- **Monitoramento**: Datadog, Nagios, Prometheus, ...;

## The Twelve-factor App

"Boas práticas para DevOps".

[https://12factor.net/](https://12factor.net/)

## O que estudaremos?

- Parte cultural e de gerenciamento é **muito** importante na implantação e manutenção do DevOps;
- Mas, focaremos no lado técnico:
  - O básico de ferramentas e processos para que vocês possam circular no ambiente de DevOps;
- Ou seja:
  - Muita administração de sistemas e ferramentas maneiras.

# Referências

CRAWFORD, Andrea. "What is DevOps". Acessado em fevereiro de 2022.
[https://www.ibm.com/cloud/learn/devops-a-complete-guide](https://www.ibm.com/cloud/learn/devops-a-complete-guide)

GERAGHTY, Tom. "The History and Evolution of DevOps", julho de 2020. Acessado em fevereiro de 2022.
[https://tomgeraghty.co.uk/index.php/the-history-and-evolution-of-devops/](https://tomgeraghty.co.uk/index.php/the-history-and-evolution-of-devops/)

GITLAB, "What is DevOps", outubro de 2019. Acessado em fevereiro de 2022.
[https://about.gitlab.com/topics/devops/](https://about.gitlab.com/topics/devops/)
