% CI/CD com Gitlab Pipeline
% Alex Kutzke
% Março 2022

# CI/CD

## Do que se trata CI/CD?

- Siga para *Continuous Integration* (CI) e *Continuous Delivery* ou *Continuous Deployment* (CD);
- É, como vimos, um movimento cultural:
  - Busca a criação de um processo **leve** para a entrega contínua da aplicação;
  * Poucos commits para cada entrega;
  * Encaixa bem com desenvolvimento web, mas pode ser utilizado em qualquer ambiente;

- Mesmo sendo uma cultura com vários pontos importantes, ainda sim, um dos aspectos chaves é a **automatização de processos**; 

## Continuous Integration (CI)

* O código é integrado rapidamente e já testado:
  - Vários desenvolvedores realizam commits em uma mesma branch;
  - Execução de testes:
  - Código integrado sem dificuldades.

## Continuous Delivery (CD)

* Após integração e testes, uma versão apta para uma entrega é gerada;
* Criação de um build, ou uma imagem docker, ou um binário, etc.
- Execução de testes:
  - Em ambiente de homologação, se for o caso;

## Continuous Deployment (CD)

* Após a integração e o *build*, automatiza o envio e a execução da nova versão em um ambiente de produção;
- Exemplo clássico: aplicação web é gerada e enviada para o servidor de produção;
- Dependendo do tipo de aplicação esse passo não é necessário;

- Essa etapa pode ser iniciada automaticamente ou exigir uma confirmação manual.

# Gilab Pipeline

## O que é?

- Plataforma online de CI/CD do Gitlab;
- Faz o mesmo trabalho que outras aplicações de gerenciamento de CI/CD (Jenkins, por exemplo);
- Fornece facilidades por ser integrado ao ambiente do Gitlab e suas diversas funcionalidades;
- Baseado em *Configuration as Code*;

## Ideia

![Fonte: GITLAB, 2022](./img/gitlab_cicd.png)

## Componentes Importantes

- **Pipeline**: Componente de mais alta abstração. Define todo o fluxo de ações para o CI/CD;
- **Job**: Define pequenas atividades a serem executadas (compilar, testar, etc);
- **Stage**: Determina o momento que um *Job* deve ser executado (jobs de testes devem ser executados após jobs de compilação).
* **Runner**: Serviço que executa *Jobs*.

## Jobs e Runners

![Fonte: GITLAB, 2022](./img/gitlab_pipeline_workflow.png)

## Detalhes

- *Jobs* de um mesmo *Stage* podem ser executados em paralelo (dependem de *runners* disponíveis);
- Se **todos** os *Jobs* de um *Stage* terminam em sucesso, o próximo *Stage* é iniciado:
  - Em caso de falha, o comportamento padrão é o pipeline ser interrompido;

## Exemplo de Pipeline comum

* Um *stage* `build`, com um *job* chamado `compile`;
* Um *stage* `test`, com um ou mais *jobs* chamados `test1`, `test2`, etc.;
* Um *stage* `staging`, com um *job* chamado `deploy-to-stage`;
* Um *stage* `production`, com um *job* chamado `deploy-to-prod`;

## Tipos de Pipelines

- Básicos: *stages* executados em sequência, um após o outro;
- Grafos Direcionados Acíclicos (DAG): pode-se determinar dependências entre *jobs*;
- Merge Request: rodam apenas para merge requests;
- Merge Result; rodam apenas em merge requests, mas consideram o código após o merge;

## Tipos de Pipelines (cont.)

* Merge trains: cria um lista ordenada para a execução dos merge requests e seus pipelines;´
* Parent-child: pipelines divididos hierarquicamente;
* Multi-project: combina pipelines em diferentes projetos;

## Requisitos

- Para a definição e execução de um Gitlab Pipeline, aos menos dois itens são necessários:
  - Um arquivo `.gitlab-ci.yml` com a definição do pipeline, seus *stages* e *jobs*;
  * Um runner disponível para execução;

# Runners

## Shared Runners

- O Gitlab fornece, em seu plano gratuito, o uso de *shared runners*;
- São *runners* compartilhados, disponibilizados pela própria plataforma;
- Se um projeto utiliza *shared runners*, a execução do pipeline irá aguardar até que exista um runner disponível;
- Recentemente, a plataforma começou a exigir o cadastro de um cartão de crédito para o uso de *shared runners*.

## Bring your own Runner

- Para contornar esse problema, sem um cartão de crédito, podemos utilizar nossos próprios *runners* locais;
- Para isso, podemos utilizar o docker;

## Rodando um Gitlab Runner localmente

- Existe uma imagem docker pronta para executar um *Gitlab Runner*:

```bash
docker run -d --name gitlab-runner --restart always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  gitlab/gitlab-runner:latest
```

Os bind mounts servem para persistir as configurações do *runner*.

## Projeto exemplo

[https://gitlab.com/das-alexkutzke/pages-example](https://gitlab.com/das-alexkutzke/pages-example)

## Registrando o runner

- É necessário registrar o *runner* para que ele possa ser utilizado com nosso projeto:

```bash
ubuntu@node1:~$ docker exec -ti gitlab-runner bash
root@914afeb69beb:/# gitlab-runner register
Runtime platform                                    arch=arm64 os=linux pid=44 revision=c6e7e194 version=14.8.2
Running in system-mode.                            
                                                   
Enter the GitLab instance URL (for example, https://gitlab.com/):
...
```

## Dados para registro

Os dados para registro podem ser encontrados nas configurações do projeto (`Settings > CI/CD > Runnesr`):

![](./img/runner_config.png)

Aproveite para desativar os *shared runners*;

## Exemplo de Registro

```bash
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.com/
Enter the registration token:
GR1348941d1zadm6AHa_kufvga-A6
Enter a description for the runner:
[914afeb69beb]: oci node 1
Enter tags for the runner (comma-separated):

Enter optional maintenance note for the runner:

Registering runner... succeeded                     runner=GR134894
Enter an executor: docker-ssh+machine, docker-ssh, parallels, ssh, docker+machine, kubernetes, custom, docker, shell, virtualbox:
docker
Enter the default Docker image (for example, ruby:2.7):
node
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded! 
```

## Testando

Para testar, basta rodar o pipeline manualmente acessando a opção `CI/CD > Pipelines` e então `Run Pipeline` na interface do projeto no Gitlab.

![](./img/run_pipeline.png)

## Testando (cont.)

Utilize o comando abaixo para observar o log do *runner* local:

```bash
docker logs -f  gitlab-runner
```

# Infrastructure as Code (IaC)

## Mudando de assunto

- Rodar runners locais é interessante;
- Mas abre espaço para conversarmos sobre outro tema: **Infrastructure as Code**;

Por que não automatizar a execução de Runner em instâncias da Oracle?

## Conceito

* Definir a infraestrutura utilizada (geralmente na cloud) em forma de código;
- Similar a *Configuration as Code* mas voltado a Infra;
- Definição da infra também entra no versionamento do projeto;
- Exemplo:
  - Criar duas VMs, com XGb de ram, Y Cpus:
    - Uma com servidor web;
    - Outra com storage para BD;

## Ferramentas

Para essa tarefa, atualmente existem várias ferramentas:

- Terraform;
- SpectralOps;
- AWS CloudFormation;
- Azure Resource Manager;
- Google Cloud Deployment Manager.

## Terraform

![[https://www.terraform.io/](https://www.terraform.io/)](./img/terraform.png)

## Terraform (cont.)

Ferramenta bastante versátil utilizada para IaC em diferentes ambientes;

- Funciona com o conceito de Providers e Modules:
  - Providers: fornecem conexões para diferentes plataformas;
  - Modules: pacotes de configurações;

## Funcionamento

- Arquivo(s) de definição da infra (extensão `.tf`);
- Mantem o estado atual da infra e, após alterações nas configurações, demonstra quais ações serão aplicadas caso efetivadas.

## Exemplo Terraform com OCI

[https://gitlab.com/das-alexkutzke/terraform-oci-example](https://gitlab.com/das-alexkutzke/terraform-oci-example)

- Provê N instâncias de VM com docker instalado;
- Na branch `runner` tem um exemplo de execução automática de *gitlab runners*.

## Execução

1. Criar uma conta na Oracle Cloud Infrastructure ([clique aqui][createaccount]);
2. Instalar o [terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli?in=terraform/oci-get-started);
3. Instalar o [OCI CLI ](https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/cliinstall.htm);
4. Configurar as credenciais OCI ([oci session authenticate](https://learn.hashicorp.com/tutorials/terraform/oci-build?in=terraform/oci-get-started));

## Execução (cont.)

5. Clonar este repositório;
6. `terraform init`
7. `terraform plan`
8. `terraform apply`

## 

De volta aos Pipelines.

# Exemplo de Pipeline

## Exemplo Simples

[https://gitlab.com/das-alexkutzke/basic-pipeline](https://gitlab.com/das-alexkutzke/basic-pipeline)

## Anatomia de um Job

Jobs:

- são definidos sob restrições de como e quando devem ser executados (*stages* e *rules*, por exemplo);
- são elementos de nome arbitrário;
- precisam conter, ao menos, uma cláusula `script`;
- não há limite para o número de jobs definidos.

## Agrupamento

Jobs similares pode ser agrupados. Basta seguir convenção na nomeação:

```yaml
build ruby 1/3:
  stage: build
  script:
    - echo "ruby1"

build ruby 2/3:
  stage: build
  script:
    - echo "ruby2"

build ruby 3/3:
  stage: build
  script:
    - echo "ruby3"
```

## Exemplo agrupamento

![Fonte: GITLAB, 2022](./img/pipeline_grouped_jobs_v14_2.png)

## Exemplo Pages

```yaml
image: ruby:latest

variables:
  JEKYLL_ENV: production
  LC_ALL: C.UTF-8

before_script:
  - gem install bundler
  - bundle install

test:
  stage: test
  script:
  - bundle exec jekyll build -d test
  artifacts:
    paths:
    - test
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

pages:
  stage: deploy
  script:
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

[https://gitlab.com/pages/jekyll/-/blob/master/.gitlab-ci.yml](https://gitlab.com/pages/jekyll/-/blob/master/.gitlab-ci.yml)

## Restringindo execução a Merge Request

- Cláusulas `rules`, `only` e `except` pode definir condições para execução de um job;
- Uma condição bastante comum é a execução em Merge Requests.

```yaml
rules:
  - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
```

## Variáveis

- É possível fazer uso de variáveis em seu `.gitlab-ci.yml` ao menos de 2 formas:
  - Cláusula `variables`;
  - Definição pela interface do projeto no Gitlab;

- O uso de variáveis é bem comum na definição de senhas ou dados sensíveis utilizados durante o pipeline.

## Exemplo Variáveis

```yaml
variables:
  TEST_VAR: "All jobs can use this variable's value"

job1:
  variables:
    TEST_VAR_JOB: "Only job1 can use this variable's value"
  script:
    - echo "$TEST_VAR" and "$TEST_VAR_JOB"
```

# Referências

GITLAB, "What is DevOps", outubro de 2019. Acessado em março de 2022.
[https://about.gitlab.com/topics/devops/](https://about.gitlab.com/topics/devops/)

GITLAB, "CI/CD Pipelines", outubro de 2019. Acessado em março de 2022.
[https://docs.gitlab.com/ee/ci/pipelines/](https://docs.gitlab.com/ee/ci/pipelines/)
